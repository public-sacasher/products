import { useEffect, useState } from "react";
import { getProducts } from '../helpers/getProducts';

/**
 * 
 * @param {*} productId the id of the Product
 * @returns the product for the corresponding id
 */
export const useFetchProduct = (productId) => {
    const [state, setState] = useState({
        data: [],
        loading: true
    });

    useEffect(() => {
        getProducts(productId).then(product => {
            setState({
                data: product,
                loading: false
            });
        })
    }, [productId]);
    
    return state;
};