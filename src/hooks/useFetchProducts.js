import { useEffect, useState } from "react";
import { getProducts } from '../helpers/getProducts';

/**
 * 
 * @returns a list of products
 */
export const useFetchProducts = () => {
    const [state, setState] = useState({
        data: [],
        loading: true
    });

    useEffect(() => {
        getProducts().then(products => {
            setState({
                data: products,
                loading: false
            });
        })
    }, []);
    
    return state;
};