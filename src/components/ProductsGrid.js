import React from 'react';
import { useFetchProducts } from '../hooks/useFetchProducts';
import { ProductGridItem } from './ProductGridItem';

/**
 * 
 * @returns The Grid to show the products
 */
export const ProductsGrid = () => {
  const {data:products, loading} = useFetchProducts();

  return (
    <>
      <h1>Products</h1>
      {loading && '<p>Cargando...</p>'}
      <div className='grid-container'>
          {
            products.map(product => {
              return(
                <ProductGridItem
                  key={product.id}
                  {...product}
                />
              );
            })
          }
      </div>
    </>
  );
}
