import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import { formatMoney } from "../helpers/formarMoney";
import './productGridItem.scss';

/**
 * 
 * @returns the the component to be displayed in the main Grid
 * it requires the id, title, price and image
 */
export const ProductGridItem = ({
  id,
  title,
  price,
  image
}) => {
  return (
    <div className="cardGridItem">
      <img src={image} alt={title}/>
      <Link to={`/${id}`} key={id}><h1>{title}</h1></Link>
      <p className="price">{formatMoney(price,'de-DE','EUR')}</p>
      <p><button>Add to Cart</button></p>
    </div> 
  );
};

ProductGridItem.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
};
