import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { useParams } from "react-router-dom";
import { formatMoney } from "../helpers/formarMoney";
import { useFetchProduct } from "../hooks/useFetchProduct";
import './productItem.scss';

/**
 * 
 * @returns the Product to be displayed in /:poductId
 */
export const ProductItem = () => {
  const params = useParams();
  const {data:product, loading} = useFetchProduct(params.poductId);
  
  const {title, price, image, description, rating} = product;

  
  /**
   * 
   * @returns the FontAwesomeIcon stars for the rating's product
   */
  const getStars = () => {
    const numberStars = Math.ceil(rating.rate);
    const stars = [];
    for (let index = 0; index < numberStars; index++) {
      stars.push(<li key={index}><FontAwesomeIcon icon={faStar} color="gold"/></li>)
    }
    if (numberStars < 5) {
      for (let index = 0; index < (5 - numberStars); index++) {
        stars.push(<li key={numberStars + index}><FontAwesomeIcon icon={faStar}/></li>)
      }
    }
    return stars;
  }

  return (
    <>
      {loading && '<p>Cargando...</p>'}
      <main>
        <div className="card">
          <div className="ard__title">
            <div className="icon">
              <a href="/#"><i className="fa fa-arrow-left"></i></a>
            </div>
            <h3>New products</h3>
          </div>
          <div className="card__body">
            <div className="half">
              <div className="featured_text">
                <p className="sub">{title}</p>
                {price && <p className="price">{formatMoney(price,'de-DE','EUR')}</p>}
              </div>
              <div className="image">
                <img src={image} alt={title}/>
              </div>
            </div>
            <div className="half">
              <div className="description">
                <p>{description}</p>
              </div>
              <span className="stock"><i className="fa fa-pen"></i> In stock</span>
              <div className="reviews">
                <ul className="stars">
                  {rating && getStars()}
                </ul>
                {rating && <span>{rating.count} reviews</span>}
              </div>
            </div>
          </div>
          <div className="card__footer">
            <div className="recommend">
              <p>Recommended by</p>
              <h3>Andrew Palmer</h3>
            </div>
            <div className="action">
              <button type="button">Add to cart</button>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};
