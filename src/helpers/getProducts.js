  /**
   * 
   * @param {*} id the id for the product to search, this param is optional, in case it is not provided returns the entire list of existing products
   * @returns the product or list of products from fakestoreapi
   */
  export const getProducts = async (id="") => {
    const url = `https://fakestoreapi.com/products/${id}`;
    const response = await fetch(url);
    const data = await response.json();

    return data;
  }