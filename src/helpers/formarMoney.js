/**
 * 
 * @param {*} num  number to be formatet
 * @param {*} locales the local option
 * @param {*} currency the currency to be formated
 * @returns the formated number
 * example input: formatMoney(15, 'en-US', 'USD')
 */
export const formatMoney= (num, locales, currency) => {
    return num.toLocaleString(locales, { style: 'currency', currency: currency });
}