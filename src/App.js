import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import { ProductItem } from './components/ProductItem';
import { ProductsGrid } from './components/ProductsGrid';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ProductsGrid />}/>
        <Route path=":poductId" element={<ProductItem />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
